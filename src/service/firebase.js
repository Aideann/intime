import firebase from 'firebase'

// Initialize Firebase
const config = {
  apiKey: 'AIzaSyD4VfKr7leNnZJFJdMmzlFyknUy5VOOX6A',
  authDomain: 'intime-b5c59.firebaseapp.com',
  databaseURL: 'https://intime-b5c59.firebaseio.com',
  projectId: 'intime-b5c59',
  storageBucket: 'intime-b5c59.appspot.com',
  messagingSenderId: '640784008827'
}
firebase.initializeApp(config)

const messaging = firebase.messaging()
messaging.requestPermission()
  .then(function () {
    console.log('Have permission')
    return messaging.getToken()
  })
  .then(function (token) {
    console.log('im getting token')
    console.log(token)
  })
  .catch(function (error) {
    console.log(error)
    console.log('Dont have permission')
  })

// export default {
//   database: firebase.database()
// }
